package co.edu.uniajc.coopfamiliar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoopfamiliarAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoopfamiliarAppApplication.class, args);
	}

}
